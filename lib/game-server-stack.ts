import * as cdk from '@aws-cdk/core';
import * as val from 'cdk-valheim';

export class GameServerStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    new val.ValheimWorld(this, 'ValheimFargate', {
      desiredCount: 1,
      environment: {
        SERVER_PASS: "something-secret"
      }
    });
  }
}
