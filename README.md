[Intro to cdk](https://docs.aws.amazon.com/cdk/latest/guide/hello_world.html)


[Connecting to a world](https://www.polygon.com/valheim-guide/22289767/server-private-community-friends-start-join-game-select-world)

Get public API for connecting:

`aws ec2 describe-network-interfaces --network-interface-ids eni-xxxxxxxx`

Dependencies:
 - [aws-cdk](https://github.com/aws/aws-cdk)
 - [cdk-valheim](https://github.com/gotodeploy/cdk-valheim)

